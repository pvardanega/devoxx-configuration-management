package com.example;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.data.repository.CrudRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@SpringBootApplication
@EnableDiscoveryClient
public class ShippingserviceApplication {

    public static void main(String[] args) {
        SpringApplication.run(ShippingserviceApplication.class, args);
    }
}

@RestController
@RefreshScope
class ShippingCostsController {

    @Value("${shipping.costs.inside}")
    private double shippingCostsInside;

    @Value("${shipping.costs.env}")
    private double shippingCostsEnv;

    @Value("${shipping.costs}")
    private double shippingCosts;

    @GetMapping("shipping-costs-inside")
    public double getShippingCostsInside() {
        return shippingCostsInside;
    }

    @GetMapping("shipping-costs-env")
    public double getShippingCostsEnv() {
        return shippingCostsEnv;
    }

    @GetMapping("shipping-costs")
    public double getShippingCosts() {
        return shippingCosts;
    }
}

@RestController
class VatController {

    @Value("${vat}")
    private double vat;

    @GetMapping("vat")
    public double getVat() {
        return vat;
    }
}

@Entity
class User {

    @Id @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @NotBlank
    private String username;

    protected User() { }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}

interface UserRepository extends CrudRepository<User, Long> {}

@RestController
class UserController {

    private final UserRepository userRepository;

    @Autowired
    UserController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @GetMapping("users")
    public Iterable<User> findAll() {
        return userRepository.findAll();
    }

    @PostMapping("users")
    public void createOne(@RequestBody @Valid User user) {
        userRepository.save(user);
    }
}