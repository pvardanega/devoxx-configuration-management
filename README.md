> Configuration refers to any value that can vary across deployments 

[Kevin Hoffman, Beyond the twelve-factor app](https://pivotal.io/beyond-the-twelve-factor-app)


That includes database credentials, connection pool settings, api key for remote access, etc. 

There are 3 levels of maturity in configuration management:

1. configuration is bundled **inside** the artifact
2. configuration is bundled **outside** the artifact
3. configuration is provided by one or multiple components from the system

# Introduction

We will go through all those 3 levels. 

## Prerequisites

### Java 8 installed 

### Maven 3 installed 

### RabbitMQ running

We can run it easily with docker: `docker run -it --rm -p 5672:5672 rabbitmq`

### Our config server should be reachable by bitbucket...

... in order to notify new commit from bitbucket to the config server and then dispatch events to tell services to 
refresh their configuration at runtime.

Just run `ngrok http 8888` and then create a webhook on bitbucket to notify on URL : `$HTTP_NGROK_PROVIDED_URL$/monitor`
(change the value $HTTP_NGROK_PROVIDED_URL$ by the one provided when we ran the `ngrok` command).

### Consul running

We can run it easily with docker: `docker run -it --rm -p 8500:8500 consul`

Then go to [http://localhost:8500](http://localhost:8500) and add one new value in the key/value store (key: `config/shippingservice/vat`, 
value `19.6`).

### Mysql running

We can run it easily with docker: `docker run -it --rm -p 3306:3306 -e MYSQL_DATABASE=devoxx -e MYSQL_ROOT_PASSWORD=root mysql`

### Vault running

We can run it easily with docker: `docker run --name vault-dev -it --rm -p 8200:8200 vault` and keep those values from the log:
> Unseal Key: 6TTwL3402ihx3YkNJAE1hhRLY+dCUHHG0CqaYKOP23o=

> Root Token: 390b58f7-37bd-ea38-6d16-5dc437546a83

Then, follow those steps:

- setup your shell to access Vault:

> export VAULT_ADDR='http://$YOUR_IP_ADDRESS$:8200/'

> export VAULT_TOKEN=$TOKEN_GIVEN_BY_VAULT_SERVER$

> alias vault='docker exec -it vault-dev vault "$@"'

- check Vault status: `vault status -address=http://$YOUR_IP_ADDRESS$:8200`

Vault is now running in **dev** mode. Please use your real IP address, not localhost or 127.0.0.1. For more information 
about Vault, please refer to the [documention](https://www.vaultproject.io/docs/).

### Clone repository and package source

> git clone git@bitbucket.org:pvardanega/devoxx-configuration-management.git

> cd devoxx-configuration-management

> mvn clean package

## General information

We should understand how Spring handles [configuration](http://docs.spring.io/spring-boot/docs/current/reference/html/boot-features-external-config.html)
 and especially the the purposes of the class [Environment.java](http://docs.spring.io/spring-framework/docs/current/javadoc-api/org/springframework/core/env/Environment.html).

Also, we use [Actuator](http://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/#production-ready) to get some 
information about the environment of the running services. For instance, we'll be able to know the configuration of the 
shipping service by browsing the URL [http://localhost:8080/env](http://localhost:8080/env), provided by actuator.

One important point to remember is that configuration sources are prioritized. We can get an overview of the priorities by
 looking at the [http://localhost:8080/env](http://localhost:8080/env). The configuration sources on **top** have the **higest 
 priority**. It is the same logic for the configuration server: profiles on top override the one at the bottom.

# Configuration bundle inside the JAR

Here, we use the `application.properties` file to store the configuration and the annotation `@Value("${my.config}")` to 
retrieve and use the configuration.

Now start the shipping service and check that the configuration is taken into account:
> cd shippingservice
 
> mvn spring-boot:run

> http://localhost:8080/shipping-costs-inside

This is a simple way of managing configuration but it has a lot drawbacks:

- we have to to re-compile, re-test, re-package, re-deploy and re-validate (at least) the package
- we have to provide one different package for each environment which is a pain in the a** to manage

# Configuration bundle outside the JAR

This time, the configuration in not stored in the package. That means we need to manage it differently. There 
are multiple solutions for that but two are really convenient and spread across companies:

- environment variables: they can be set by our favorite configuration management tool like Ansible, Chef, Puppet, Saltstack, etc. 
This solution is used by cloud platforms like Cloud Foundry or Heroku to allow developers to configure their applications 
and also to bind services between each other (database, message broker, remote service, etc.)
- configuration files (yaml, properties, whatever we like): they also can be pushed by configuration management tool like Ansible, Chef, Puppet, Saltstack, etc.

Now start the shipping service and check that the configuration is taken into account:
> cd shippingservice
 
> mvn spring-boot:run 

> http://localhost:8080/shipping-costs-env

This solution needs a bit a more work, especially setting up the environment variables/configuration files but it is more flexible:

- we still need to restart the application
- configuration can be stored in a repository to take advantage DVCS features (history) and avoid being in a situation where
 nobody knows who and why a configuration has been changed. 

# Configuration is provided by one or multiple components from the system

This solution is the more flexible one but also the more complex to setup and maintain. What's cool with Spring is that 
we can use multiple configuration sources by mixing configuration server, Consul and Vault with Spring Cloud.

## Configuration server

This web application can read any local or remote repositories. It's pretty simple to make it up and running, just create 
a Spring boot application with the dependency `spring-cloud-config-server`. Then, we need to setup the configuration server:

- add the annotation `@EnableConfigServer` somewhere in the classpath
- configure the configuration source by setting the property `spring.cloud.config.server.git.uri` in `bootstrap.[properties|yml]`
 
Now start the config service and check that the configuration is taken into account:
> cd configservice
 
> mvn spring-boot:run 

> http://localhost:8888/shippingservice/default

> http://localhost:8888/shippingservice/production

From their, we can see that configuration is loaded. Each time we request for one of the configuration using http, the 
config server reads the repository to provide fresh configuration.

If we want to play a bit more with the configuration, we can take a look at the [documention](https://cloud.spring.io/spring-cloud-config/spring-cloud-config.html). 
We can configure many things.

## Configuration config server clients

Now that the configuration server is up and running, we need to setup the shipping service to use it. For that, we just 
need to include the dependency (`spring-cloud-starter-config`) and configure the property `spring.cloud.config.uri` in 
the file `bootstrap.[properties|yml]` to point to the config server.
 
We also need to configure the property `spring.application.name` with the same name that is used to name the configuration 
files in the repository. Spring Cloud in general uses this value to recognize a service. For instance, when we use service
 discovery with Eureka, we need to setup this value because the service will be identified with this value in Eureka.
 
We can restart the shipping service with the profile we want to see that the configuration changes corresponding to the 
profile

## Refresh configuration at runtime

Right now, if we commit a configuration change in the repository, the config server will be notified but the shipping service 
won't be refreshed.

To achieve that, we first need to make our component refreshable by annotating them with `@RefreshScope`. Then, there 
are 2 ways to refresh the service:

- using refresh endpoint provided by Actuator
- using an event bus

### Using actuator

We can now execute the following HTTP request:
> curl -X POST http://localhost:8080/refresh

> http://localhost:8080/shipping-costs

and observed that the shipping service configuration has been refreshed.

This is a manual task, that can be scripted, but there is a more convenient way to refresh configuration at runtime: event bus.

### Using an event bus

The event bus is using RabbitMQ thanks to the dependency `spring-cloud-starter-bus-amqp`, which is added in both modules 
(shipping and config service). Just by doing that, the config server will push events in the bus when it will receive notifications
 from the repository webhook. And... that's it!
 
Note: please deactivate all vault dependencies. As they are still in development, they conflict with bus dependency.
 
We restart both applications, config server first, and commit a change in the configuration repository. Then refresh [http://localhost:8080/shipping-costs](http://localhost:8080/shipping-costs),
 it should be updated to the value we've just set.
 
That is really cool but one problem remains: security. It is a really bad practice to put credentials in a repository.

# Other configuration sources

We will use Vault to provide sensible configuration. 

Notice that if we check [http://localhost:8080/env](http://localhost:8080/env) during this section, we'll see that:

- multiple configuration sources can be used at the same time
- the order of the configuration sources matters (the one on top has the highest priority)

## Vault

Vault is a nice tool to manage credentials. It allows us to avoid taking care of database credentials for instance when 
it is well configured. This is good for security. 

Please, note that Spring Cloud Vault is still in development, there is no release yet. That's why we need to deactivate 
spring cloud bus ampq (see pom.xml).

Here are the following steps to configure the shipping service to connect to Vault:

1. add `spring-cloud-vault-starter-config` and `spring-cloud-vault-config-databases` with the latest version (actual is 1.0.0.M1)
2. add `spring-boot-starter-data-jpa` and `mysql-connector-java`
3. then configure the connection to Vault in the file `bootstrap.[properties|yml]`

``` yml
spring:  
  cloud:  
    vault:
        scheme: http
        token: $TOKEN_GIVEN_BY_VAULT_SERVER$
      mysql:
        enabled: true
        backend: mysql
        role: readonly
        username-property: spring.datasource.username
        password-property: spring.datasource.password
```

First, we setup Spring Cloud Vault to use HTTP to communicate with Vault by using a valid token generated by Vault. Then,
 we setup Spring Cloud Vault to use the [Mysql secret backend](http://cloud.spring.io/spring-cloud-vault-config/spring-cloud-vault-config.html#vault.config.backends.mysql).
 We use the role defined earlier and we bind spring datasource configuration to vault mysql credentials.

We need to configure Vault to use Mysql secret backend:

> vault mount -address=http://$YOUR_IP_ADDRESS$:8200 mysql

> vault write -address=http://$YOUR_IP_ADDRESS$:8200 mysql/config/connection connection_url="root:root@tcp($YOUR_IP_ADDRESS$:3306)/"

> vault write -address=http://$YOUR_IP_ADDRESS$:8200 mysql/roles/readonly sql="CREATE USER '{{name}}'@'%' IDENTIFIED BY '{{password}}';GRANT ALL ON *.* TO '{{name}}'@'%';"

Spring also needs us to setup the datasource URL. To do so, we will store it in Vault because it is sensible:
> vault write -address=http://$YOUR_IP_ADDRESS$:8200 secret/shippingservice spring.datasource.url=jdbc:mysql://localhost:3306/devoxx spring.jpa.generate-ddl=true

Then, we restart the application and play with the API to create and retrieve users:

> http://localhost:8080/users 

> curl -X POST http://localhost:8080/users -H 'Content-Type: application/json' -d '{ "username": "Pierre" }'

## Consul

Finally, we can also use Consul to provide some configuration. It can be convenient if we already use it for its main 
feature: service discovery. To use it, add the dependency `spring-cloud-starter-consul-config` and add the annotation 
`@EnableDiscoveryClient` somewhere in the classpath.

Restart the application and request for the URL
> http://localhost:8080/vat
